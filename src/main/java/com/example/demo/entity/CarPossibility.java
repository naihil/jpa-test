package com.example.demo.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "car_possibility")
@SequenceGenerator(name = "SeqCarPossibilityId", sequenceName = "car_possibility_seq", allocationSize = 1)
@Data
public class CarPossibility {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SeqCarPossibilityId")
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "title")
	private String title;

	@OneToMany(mappedBy = "carPossibility", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CarPossibilitySparePart> spareParts = new ArrayList<>();

	public void addSparePart(CarPossibilitySparePart sparePart) {
		sparePart.setCarPossibility(this);
		spareParts.add(sparePart);
	}

	public void delSparePart(CarPossibilitySparePart sparePart) {
		spareParts.remove(sparePart);
		sparePart.setCarPossibility(null);
	}
}
