package com.example.demo.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "car_possibility_spare_part")
@SequenceGenerator(name = "SeqCarPossibSparePart", sequenceName = "car_possibility_spare_part_seq", allocationSize = 1)
@Data
public class CarPossibilitySparePart {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SeqCarPossibSparePart")
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "title")
	private String title;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "car_possibility_id")
	@ToString.Exclude
	private CarPossibility carPossibility;

	public CarPossibilitySparePart() {
	}

	public CarPossibilitySparePart(String title) {
		this.title = title;
	}
}
