package com.example.demo.service;

import com.example.demo.entity.CarPossibility;
import com.example.demo.entity.CarPossibilitySparePart;
import com.example.demo.repository.CarPossibilityRepository;
import com.example.demo.repository.CarPossibilitySparePartRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TestService {
	private final CarPossibilityRepository carPossibilityRepository;
	private final CarPossibilitySparePartRepository carPossibilitySparePartRepository;

	@Transactional
	public void insertPossibility() {
		CarPossibility carPossibility = new CarPossibility();
		carPossibility.setTitle("test");
		carPossibility = carPossibilityRepository.save(carPossibility);

		log.info("[insertPossibility][RESULT] result[{}]", carPossibility);
	}

	@Transactional
	public void addSpareParts() {
		CarPossibility carPossibility = carPossibilityRepository.getOne(1L);
		carPossibility.addSparePart(new CarPossibilitySparePart("part 1"));
		carPossibility.addSparePart(new CarPossibilitySparePart("part 2"));
		carPossibility.addSparePart(new CarPossibilitySparePart("part 3"));
		carPossibility = carPossibilityRepository.save(carPossibility);

		log.info("[addSpareParts][RESULT] result[{}]", carPossibility);
	}

	@Transactional
	public void info() {
		final CarPossibility one = carPossibilityRepository.getOne(1L);
		log.info("[INFO] {}", one);
	}

	@Transactional
	public void delSparePart() {
		CarPossibility carPossibility = carPossibilityRepository.getOne(1L);
		CarPossibilitySparePart part = carPossibilitySparePartRepository.getOne(1L);

		carPossibility.delSparePart(part);
		carPossibilityRepository.save(carPossibility);
	}
}
