package com.example.demo.repository;

import com.example.demo.entity.CarPossibilitySparePart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarPossibilitySparePartRepository extends JpaRepository<CarPossibilitySparePart, Long> {
}
